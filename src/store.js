import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    contacts: [
      { id: 0,
        name: 'Mario',
        surname: 'Mariani',
        number: '123456',
        imagine: '../assets/rubrica.png'
      },
      { id: 1,
        name: 'Giorgio',
        surname: 'Bisontii',
        number: '115246',
        imagine: '../assets/rubrica.png'
      },
      { id: 2,
        name: 'Carla',
        surname: 'Savoiardi',
        number: '846291',
        imagine: '../assets/rubrica.png'
      },
      { id: 3,
        name: 'Misa',
        surname: 'Verdii',
        number: '346132',
        imagine: '../assets/rubrica.png'
      }
    ]
  },
  mutations: {
    ADD_LINK: (state, contact) => {
      state.contacts.push(contact)
    }
  },
  actions: {

  },
  modules: {}
})
